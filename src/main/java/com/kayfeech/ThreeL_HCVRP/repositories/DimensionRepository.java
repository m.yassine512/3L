package com.kayfeech.ThreeL_HCVRP.repositories;


import com.kayfeech.ThreeL_HCVRP.entities.Dimension;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DimensionRepository extends JpaRepository<Dimension, Long>{

}