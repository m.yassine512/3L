package com.kayfeech.ThreeL_HCVRP.repositories;


import com.kayfeech.ThreeL_HCVRP.entities.Depot;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepotRepository extends JpaRepository<Depot, Long>{

}