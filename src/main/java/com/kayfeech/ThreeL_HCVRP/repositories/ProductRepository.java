package com.kayfeech.ThreeL_HCVRP.repositories;


import com.kayfeech.ThreeL_HCVRP.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long>{

}