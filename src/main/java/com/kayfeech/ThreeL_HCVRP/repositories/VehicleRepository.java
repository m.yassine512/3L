package com.kayfeech.ThreeL_HCVRP.repositories;


import com.kayfeech.ThreeL_HCVRP.entities.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VehicleRepository extends JpaRepository<Vehicle, Long>{

}