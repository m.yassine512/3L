package com.kayfeech.ThreeL_HCVRP.repositories;


import com.kayfeech.ThreeL_HCVRP.entities.Route;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RouteRepository extends JpaRepository<Route, Long>{

}