package com.kayfeech.ThreeL_HCVRP.entities;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.util.*;

/**
 * 
 */
@Entity
public class Product extends BaseEntity {


    /**
     * 
     */
    private String nameProduct;
    private Double weightProduct;

    /**
     * 
     */
    private Double volumeProduct;

    /**
     * 
     */
    @OneToOne
    private Dimension dimensionProduct;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Product product = (Product) o;
        return Objects.equals(nameProduct, product.nameProduct) &&
                Objects.equals(weightProduct, product.weightProduct) &&
                Objects.equals(volumeProduct, product.volumeProduct) &&
                Objects.equals(dimensionProduct, product.dimensionProduct);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), weightProduct, volumeProduct, dimensionProduct);
    }

    public Double getWeightProduct() {
        return weightProduct;
    }

    public void setWeightProduct(Double weightProduct) {
        this.weightProduct = weightProduct;
    }

    public Double getVolumeProduct() {
        return volumeProduct;
    }

    public void setVolumeProduct(Double volumeProduct) {
        this.volumeProduct = volumeProduct;
    }

    public Dimension getDimensionProduct() {
        return dimensionProduct;
    }

    public void setDimensionProduct(Dimension dimensionProduct) {
        this.dimensionProduct = dimensionProduct;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public void setNameProduct(String nameProduct) {
        this.nameProduct = nameProduct;
    }

    @Override
    public String toString() {
        return "Product{" +
                "nameProduct=" + nameProduct +
                "weightProduct=" + weightProduct +
                ", volumeProduct=" + volumeProduct +
                ", dimensionProduct=" + dimensionProduct +
                ", id=" + id +
                '}';
    }
}