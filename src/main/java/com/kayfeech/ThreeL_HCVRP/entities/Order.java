package com.kayfeech.ThreeL_HCVRP.entities;

import javax.persistence.*;
import java.util.*;

/**
 * 
 */
@Entity
public class Order extends BaseEntity {


    /**
     * 
     */
    private Double volumeOrder;

    /**
     * 
     */
    private Double weightOrder;

    /**
     * 
     */
    @ManyToOne(fetch = FetchType.LAZY)
    private Client client;

    @ManyToMany
    @JoinTable(name = "LIGNE_ORDER", joinColumns = { @JoinColumn(name = "PRODUCT_ID") }, inverseJoinColumns = { @JoinColumn(name = "ORDER_ID") })
    private List<Product> products;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Order order = (Order) o;
        return Objects.equals(volumeOrder, order.volumeOrder) &&
                Objects.equals(weightOrder, order.weightOrder) &&
                Objects.equals(client, order.client);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), volumeOrder, weightOrder, client);
    }

    public Double getVolumeOrder() {
        return volumeOrder;
    }

    public void setVolumeOrder(Double volumeOrder) {
        this.volumeOrder = volumeOrder;
    }

    public Double getWeightOrder() {
        return weightOrder;
    }

    public void setWeightOrder(Double weightOrder) {
        this.weightOrder = weightOrder;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "Order{" +
                "volumeOrder=" + volumeOrder +
                ", weightOrder=" + weightOrder +
                ", client=" + client +
                ", id=" + id +
                '}';
    }
}