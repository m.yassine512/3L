package com.kayfeech.ThreeL_HCVRP.entities;

import javax.persistence.Entity;
import java.util.*;

/**
 * 
 */
@Entity
public class Dimension extends BaseEntity {

    /**
     * 
     */
    private float length;

    /**
     * 
     */
    private float width;

    /**
     * 
     */
    private float height;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Dimension dimension = (Dimension) o;
        return Float.compare(dimension.length, length) == 0 &&
                Float.compare(dimension.width, width) == 0 &&
                Float.compare(dimension.height, height) == 0;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), length, width, height);
    }

    public float getLength() {
        return length;
    }

    public void setLength(float lenght) {
        this.length = lenght;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "Dimension{" +
                "length=" + length +
                ", width=" + width +
                ", height=" + height +
                ", id=" + id +
                '}';
    }
}