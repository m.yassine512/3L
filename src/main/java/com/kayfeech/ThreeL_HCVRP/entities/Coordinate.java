package com.kayfeech.ThreeL_HCVRP.entities;

import javax.persistence.Entity;
import java.util.*;

/**
 * 
 */
@Entity
public class Coordinate extends BaseEntity {


    /**
     * 
     */
    private Double latitude;

    /**
     * 
     */
    private Double longitude;

    private String adresse;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Coordinate that = (Coordinate) o;
        return Objects.equals(latitude, that.latitude) &&
                Objects.equals(longitude, that.longitude) &&
                Objects.equals(adresse, that.adresse);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(),adresse, latitude, longitude);
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    @Override
    public String toString() {
        return "Coordinate{" +
                "adresse=" + adresse +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", id=" + id +
                '}';
    }
}