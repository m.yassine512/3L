package com.kayfeech.ThreeL_HCVRP.entities;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.util.*;

/**
 * 
 */
@Entity
public class Depot extends BaseEntity {

    /**
     * 
     */
    private int minClientPerRoute;

    /**
     * 
     */
    private String nameDepot;

    /**
     * 
     */
    @OneToOne
    private Coordinate coordinateDepot;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Depot depot = (Depot) o;
        return minClientPerRoute == depot.minClientPerRoute &&
                Objects.equals(nameDepot, depot.nameDepot) &&
                Objects.equals(coordinateDepot, depot.coordinateDepot);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), minClientPerRoute, nameDepot, coordinateDepot);
    }

    public int getMinClientPerRoute() {
        return minClientPerRoute;
    }

    public void setMinClientPerRoute(int minClientPerRoute) {
        this.minClientPerRoute = minClientPerRoute;
    }

    public String getNameDepot() {
        return nameDepot;
    }

    public void setNameDepot(String nameDepot) {
        this.nameDepot = nameDepot;
    }

    public Coordinate getCoordinateDepot() {
        return coordinateDepot;
    }

    public void setCoordinateDepot(Coordinate coordinateDepot) {
        this.coordinateDepot = coordinateDepot;
    }

    @Override
    public String toString() {
        return "Depot{" +
                "minClientPerRoute=" + minClientPerRoute +
                ", nameDepot='" + nameDepot + '\'' +
                ", coordinateDepot=" + coordinateDepot +
                ", id=" + id +
                '}';
    }
}