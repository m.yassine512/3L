package com.kayfeech.ThreeL_HCVRP.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.util.*;

/**
 * 
 */
@Entity
public class Route extends BaseEntity {


    /**
     * 
     */
    private Long totalDistance;

    /**
     * 
     */
    private Double totalCost;

    /**
     * 
     */
    private int nbrCust;

    /**
     * 
     */
    @ManyToOne(fetch = FetchType.LAZY)
    private Depot startDepot;

    /**
     * 
     */
    @ManyToOne(fetch = FetchType.LAZY)
    private Depot endDepot;

    /**
     *
     */
    @ManyToOne(fetch = FetchType.LAZY)
    private Vehicle vehicle;



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Route route = (Route) o;
        return nbrCust == route.nbrCust &&
                Objects.equals(totalDistance, route.totalDistance) &&
                Objects.equals(totalCost, route.totalCost) &&
                Objects.equals(startDepot, route.startDepot) &&
                Objects.equals(endDepot, route.endDepot);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), totalDistance, totalCost, nbrCust, startDepot, endDepot);
    }

    public Long getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(Long totalDistance) {
        this.totalDistance = totalDistance;
    }

    public Double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Double totalCost) {
        this.totalCost = totalCost;
    }

    public int getNbrCust() {
        return nbrCust;
    }

    public void setNbrCust(int nbrCust) {
        this.nbrCust = nbrCust;
    }

    public Depot getStartDepot() {
        return startDepot;
    }

    public void setStartDepot(Depot startDepot) {
        this.startDepot = startDepot;
    }

    public Depot getEndDepot() {
        return endDepot;
    }

    public void setEndDepot(Depot endDepot) {
        this.endDepot = endDepot;
    }

    @Override
    public String toString() {
        return "Route{" +
                "totalDistance=" + totalDistance +
                ", totalCost=" + totalCost +
                ", nbrCust=" + nbrCust +
                ", startDepot=" + startDepot +
                ", endDepot=" + endDepot +
                ", id=" + id +
                '}';
    }
}