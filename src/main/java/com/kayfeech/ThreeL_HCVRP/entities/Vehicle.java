package com.kayfeech.ThreeL_HCVRP.entities;

import javax.persistence.Entity;
import java.util.*;

/**
 * 
 */
@Entity
public class Vehicle extends BaseEntity {

    /**
     * 
     */
    private String nameVehicle;
    private String matricule;
    private Double maxWeight;

    /**
     * 
     */
    private Double fixedCost;

    /**
     * 
     */
    private Double operatingCostPerMetre;

    /**
     * 
     */
    private Double volumeVehicle;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Vehicle vehicle = (Vehicle) o;
        return Objects.equals(nameVehicle, vehicle.nameVehicle) &&
                Objects.equals(matricule, vehicle.matricule) &&
                Objects.equals(maxWeight, vehicle.maxWeight) &&
                Objects.equals(fixedCost, vehicle.fixedCost) &&
                Objects.equals(operatingCostPerMetre, vehicle.operatingCostPerMetre) &&
                Objects.equals(volumeVehicle, vehicle.volumeVehicle);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(),nameVehicle,matricule, maxWeight, fixedCost, operatingCostPerMetre, volumeVehicle);
    }

    public Double getMaxWeight() {
        return maxWeight;
    }

    public void setMaxWeight(Double maxWeight) {
        this.maxWeight = maxWeight;
    }

    public Double getFixedCost() {
        return fixedCost;
    }

    public void setFixedCost(Double fixedCost) {
        this.fixedCost = fixedCost;
    }

    public Double getOperatingCostPerMetre() {
        return operatingCostPerMetre;
    }

    public void setOperatingCostPerMetre(Double operatingCostPerMetre) {
        this.operatingCostPerMetre = operatingCostPerMetre;
    }

    public Double getVolumeVehicle() {
        return volumeVehicle;
    }

    public void setVolumeVehicle(Double volumeVehicle) {
        this.volumeVehicle = volumeVehicle;
    }

    public String getNameVehicle() {
        return nameVehicle;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setNameVehicle(String nameVehicle) {
        this.nameVehicle = nameVehicle;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "nameVehicle=" + nameVehicle +
                "matricule=" + matricule +
                "maxWeight=" + maxWeight +
                ", fixedCost=" + fixedCost +
                ", OperatingCostPerMetre=" + operatingCostPerMetre +
                ", volumeVehicle=" + volumeVehicle +
                ", id=" + id +
                '}';
    }
}