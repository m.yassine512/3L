package com.kayfeech.ThreeL_HCVRP.entities;


import javax.persistence.*;
import java.util.Objects;

@Entity
public class Client extends BaseEntity{

	/**
	 *
	 */
	private String nameClient;

	/**
	 *
	 */
	@OneToOne
	private Coordinate coordinateClient;


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Client client = (Client) o;
		return Objects.equals(nameClient, client.nameClient) &&
				Objects.equals(coordinateClient, client.coordinateClient);
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), nameClient, coordinateClient);
	}

	public Client(String nameClient) {
		this.nameClient = nameClient;
	}

	public Client() {
		this.nameClient = nameClient;
	}

	public String getNameClient() {
		return nameClient;
	}

	public void setNameClient(String nameClient) {
		this.nameClient = nameClient;
	}

	public Coordinate getCoordinateClient() {
		return coordinateClient;
	}

	public void setCoordinateClient(Coordinate coordinateClient) {
		this.coordinateClient = coordinateClient;
	}

	@Override
	public String toString() {
		return "Client{" +
				"nameClient='" + nameClient + '\'' +
				", coordinateClient=" + coordinateClient +
				", id=" + id +
				'}';
	}
}