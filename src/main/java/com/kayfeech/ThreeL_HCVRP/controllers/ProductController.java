package com.kayfeech.ThreeL_HCVRP.controllers;

import com.kayfeech.ThreeL_HCVRP.entities.Product;
import com.kayfeech.ThreeL_HCVRP.repositories.DimensionRepository;
import com.kayfeech.ThreeL_HCVRP.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="http://localhost:4200")
public class ProductController {
	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private DimensionRepository dimensionRepository;

	@GetMapping("/products")
	public List<Product> getProducts() {

		return productRepository.findAll();
	}

	@GetMapping("/product/{id}")
	public Optional<Product> getProduct(@PathVariable Long id) {
		return productRepository.findById(id);
	}

	@DeleteMapping("/product/{id}")
	public boolean deleteProduct(@PathVariable Long id) {
		productRepository.deleteById(id);
		return true;
	}

	@PutMapping("/product")
	public Product updateProduct(@RequestBody Product product) {

		dimensionRepository.save(product.getDimensionProduct());
		return productRepository.save(product);
	}

	@PostMapping("/product")
	public Product createProduct(@RequestBody Product product) {
		System.out.println(product.getDimensionProduct().toString());
		System.out.println(product.toString());
		dimensionRepository.save(product.getDimensionProduct());
		return productRepository.save(product);
	}

}