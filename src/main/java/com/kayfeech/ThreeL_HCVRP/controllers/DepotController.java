package com.kayfeech.ThreeL_HCVRP.controllers;

import com.kayfeech.ThreeL_HCVRP.entities.Depot;
import com.kayfeech.ThreeL_HCVRP.repositories.DepotRepository;
import com.kayfeech.ThreeL_HCVRP.repositories.CoordinateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="http://localhost:4200")
public class DepotController {
	@Autowired
	private DepotRepository depotRepository;
	@Autowired
	private CoordinateRepository coordinateRepository;

	@GetMapping("/depots")
	public List<Depot> getDepots() {

		return depotRepository.findAll();
	}

	@GetMapping("/depot/{id}")
	public Optional<Depot> getDepot(@PathVariable Long id) {
		return depotRepository.findById(id);
	}

	@DeleteMapping("/depot/{id}")
	public boolean deleteDepot(@PathVariable Long id) {
		depotRepository.deleteById(id);
		return true;
	}

	@PutMapping("/depot")
	public Depot updateDepot(@RequestBody Depot depot) {
			coordinateRepository.save(depot.getCoordinateDepot());
		return depotRepository.save(depot);
	}

	@PostMapping("/depot")
	public Depot createDepot(@RequestBody Depot depot) {
		coordinateRepository.save(depot.getCoordinateDepot());
		return depotRepository.save(depot);
	}

}