package com.kayfeech.ThreeL_HCVRP.controllers;

import java.util.List;
import java.util.Optional;

import com.kayfeech.ThreeL_HCVRP.repositories.ClientRepository;
import com.kayfeech.ThreeL_HCVRP.entities.Client;
import com.kayfeech.ThreeL_HCVRP.repositories.CoordinateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="http://localhost:4200")
public class ClientController {
	@Autowired
	private ClientRepository clientRepository;
	@Autowired
	private CoordinateRepository coordinateRepository;

	@GetMapping("/clients")
	public List<Client> getClients() {

		return clientRepository.findAll();
	}

	@GetMapping("/client/{id}")
	public Optional<Client> getClient(@PathVariable Long id) {
		return clientRepository.findById(id);
	}

	@DeleteMapping("/client/{id}")
	public boolean deleteClient(@PathVariable Long id) {
		clientRepository.deleteById(id);
		return true;
	}

	@PutMapping("/client")
	public Client updateClient(@RequestBody Client client) {
			coordinateRepository.save(client.getCoordinateClient());
		return clientRepository.save(client);
	}

	@PostMapping("/client")
	public Client createClient(@RequestBody Client client) {
		coordinateRepository.save(client.getCoordinateClient());
		return clientRepository.save(client);
	}

}