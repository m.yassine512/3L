package com.kayfeech.ThreeL_HCVRP.controllers;

import com.kayfeech.ThreeL_HCVRP.entities.Vehicle;
import com.kayfeech.ThreeL_HCVRP.repositories.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="http://localhost:4200")
public class VehicleController {
	@Autowired
	private VehicleRepository vehicleRepository;

	@GetMapping("/vehicles")
	public List<Vehicle> getVehicles() {

		return vehicleRepository.findAll();
	}

	@GetMapping("/vehicle/{id}")
	public Optional<Vehicle> getVehicle(@PathVariable Long id) {
		return vehicleRepository.findById(id);
	}

	@DeleteMapping("/vehicle/{id}")
	public boolean deleteVehicle(@PathVariable Long id) {
		vehicleRepository.deleteById(id);
		return true;
	}

	@PutMapping("/vehicle")
	public Vehicle updateVehicle(@RequestBody Vehicle vehicle) {
		return vehicleRepository.save(vehicle);
	}

	@PostMapping("/vehicle")
	public Vehicle createVehicle(@RequestBody Vehicle vehicle) {
		return vehicleRepository.save(vehicle);
	}

}