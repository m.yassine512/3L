package com.kayfeech.ThreeL_HCVRP;


import com.kayfeech.ThreeL_HCVRP.entities.Client;
import com.kayfeech.ThreeL_HCVRP.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThreeL_HCVRPApplication implements CommandLineRunner {
	@Autowired
	private ClientRepository clientRepository;
	
	
	public static void main(String[] args) {
		SpringApplication.run(ThreeL_HCVRPApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
	}
}