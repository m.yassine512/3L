import {Coordinate} from "./coordinate";
import {BaseEntity} from "./baseEntity";

export class Depot extends BaseEntity{

    nameDepot:string;
    coordinateDepot:Coordinate;

    constructor(nameDepot: string, coordinateDepot: Coordinate) {
        super();
        this.nameDepot = nameDepot;
        this.coordinateDepot = coordinateDepot;
    }
}