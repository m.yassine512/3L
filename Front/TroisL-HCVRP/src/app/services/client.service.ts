import { Injectable } from '@angular/core';
import {Http,Headers, Response, RequestOptions} from "@angular/http";
import { map } from "rxjs/operators";
import {catchError} from "rxjs/internal/operators";
import {throwError} from "rxjs/index";
import {Client} from "../client";
import {HttpHeaders} from "@angular/common/http";
import {Coordinate} from "../coordinate";

const headers = new HttpHeaders()
    .set("Access-Control-Allow-Origin", "*");

@Injectable()
export class ClientService {
    constructor(private _http:Http) {

    }


  private rootUrl:string='/api';
  private headers = new Headers({'Content-Type':'application/json'});
  private options = new RequestOptions({headers:this.headers});
  private client = new Client('',new Coordinate(0,0,''));


  getClients(){
    return this._http.get(this.rootUrl+'/clients',this.options)
        .pipe(map((response:Response)=>response.json()),
            catchError(this.errorHandler));
  }

  getClient(id:Number){
        return this._http.get(this.rootUrl+'/client/'+id,this.options)
            .pipe(map((response:Response)=>response.json()),
                catchError(this.errorHandler));
  }


  deleteClient(id:Number){
        return this._http.delete(this.rootUrl+'/client/'+id,this.options)
            .pipe(map((response:Response)=>response.json()),
              catchError(this.errorHandler));
  }

  createClient(client:Client){
        return this._http.post(this.rootUrl+'/client',JSON.stringify(client),this.options)
            .pipe(map((response:Response)=>response.json()),
             catchError(this.errorHandler));
  }

  updateClient(client:Client){
        return this._http.put(this.rootUrl+'/client',JSON.stringify(client),this.options)
            .pipe(map((response:Response)=>response.json()),
            catchError(this.errorHandler));
  }

  errorHandler(error:Response){
    return throwError(error||'Server ERROR' )
  }

  setClient(client:Client){
      this.client=client;
  }
  clientGetter(){
      return this.client;
  }

}
