import {BaseEntity} from "./baseEntity";
import {Depot} from "./depot";
import {Vehicle} from "./vehicle";

export class laRoute extends BaseEntity {


    totalDistance:Number;
    totalCost:Number;
    nbrCust:Number;
    startDepot:Depot;
    endDepot:Depot;
    vehicle:Vehicle;
}