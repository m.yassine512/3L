import {Coordinate} from "./coordinate";

export class Waypoint {


    public location: {};
    stopover: boolean=true;


    constructor(lat : Number, lng : Number) {
        this.location={lat,lng};
    }
}