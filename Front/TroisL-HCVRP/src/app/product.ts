import {BaseEntity} from "./baseEntity";
import {Dimension} from "./dimension";

export class Product extends BaseEntity {

    nameProduct:string;
    weightProduct:Number;
    volumeProduct:Number;
    dimensionProduct:Dimension;

    constructor(dimensionProduct: Dimension) {
        super();
        this.dimensionProduct = dimensionProduct;
    }
}