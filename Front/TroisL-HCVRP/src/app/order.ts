import {BaseEntity} from "./baseEntity";
import {Product} from "./product";
import {Client} from "./client";

export class Order extends BaseEntity {


    volumeOrder:Number;
    weightOrder:Number;
    client:Client;
    products:Product[];
}