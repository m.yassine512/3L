import { Component, OnInit } from '@angular/core';
import {Product} from "../../product";
import {ProductService} from "../../services/product.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.css']
})
export class ListProductsComponent implements OnInit {
  private products:Product[];

  constructor(private _productService:ProductService, private _router:Router) { }

  ngOnInit() {
      this._productService.getProducts().subscribe((products)=>{
          this.products=products;
      },(error)=>{
          console.log(error);
      });
  }

    addProduct(){this._router.navigate(['/products','product','']);}

    updateProduct(id){

        this._router.navigate(['/products','product',id]);
    }

    deleteProduct(product){
        this._productService.deleteProduct(product.id).subscribe((data)=>{
            this.products.splice(this.products.indexOf(product),1);
        },(error)=>{
            console.log(error);
        });
    }


}
