import { Component, OnInit } from '@angular/core';
import {Vehicle} from "../../vehicle";
import {Router} from "@angular/router";
import {VehicleService} from "../../services/vehicle.service";

@Component({
  selector: 'app-list-vehicles',
  templateUrl: './list-vehicles.component.html',
  styleUrls: ['./list-vehicles.component.css']
})
export class ListVehiclesComponent implements OnInit {
    private vehicles:Vehicle[];

    constructor(private _vehicleService:VehicleService, private _router:Router ) {

    }

    ngOnInit() {

        this._vehicleService.getVehicles().subscribe((vehicles)=>{
            this.vehicles=vehicles;
        },(error)=>{
            console.log(error);
        });
    }

    addVehicle(){this._router.navigate(['/vehicles','vehicle','']);}

    updateVehicle(id){

        this._router.navigate(['/vehicles','vehicle',id]);
    }

    deleteVehicle(vehicle){
        this._vehicleService.deleteVehicle(vehicle.id).subscribe((data)=>{
            this.vehicles.splice(this.vehicles.indexOf(vehicle),1);
        },(error)=>{
            console.log(error);
        });
    }


}
