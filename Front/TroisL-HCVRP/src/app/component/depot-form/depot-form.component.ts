import { Component, OnInit } from '@angular/core';


import {Router} from "@angular/router";
import {Depot} from "../../depot";
import {DepotService} from "../../services/depot.service";
import { ActivatedRoute } from '@angular/router';
import {Coordinate} from "../../coordinate";

@Component({
  selector: 'app-depot-form',
  templateUrl: './depot-form.component.html',
  styleUrls: ['./depot-form.component.css']
})
export class DepotFormComponent implements OnInit {
    private depot:Depot=new Depot('',new Coordinate(0,0,''));
    private id:Number;
    private bol=true;

    constructor(private _depotServise:DepotService, private _router:Router,private _route:ActivatedRoute) {

    }


    ngOnInit(){
    this.callDepot();


    }
    depotLocation(event){
        console.log(event);
        this.depot.coordinateDepot.latitude = event.coords.lat;
        this.depot.coordinateDepot.longitude = event.coords.lng;
        this.bol=true;
    }
    saveDepot(){
        if(this.depot.id==undefined){
            this._depotServise.createDepot(this.depot).subscribe((depot)=>{
                console.log(depot);
            },(error)=>{
                console.log(error);
            });
            this._router.navigate(["/"]);
        }else {this._depotServise.updateDepot(this.depot).subscribe((depot)=>{
            console.log(depot);
        },(error)=>{
            console.log(error);
        });this._router.navigate(["/"]);}
    }

    callDepot(){
        this._route.params.subscribe((params) => {
            this.id = +params['id'];
            if(this.id){
                this._depotServise.getDepot(this.id).subscribe((depot)=>{
                    this.depot=depot;
                },(error)=>{
                    console.log(error);
                });
            }else{
                this.depot.coordinateDepot.latitude = 33.581053564669844;
                this.depot.coordinateDepot.longitude = -7.594315135130387;
                this.bol=false;
            }
        });
    }
}

