import { Component, OnInit } from '@angular/core';
import {DepotService} from "../../services/depot.service";
import {Router} from "@angular/router";

import {Depot} from "../../depot";

@Component({
  selector: 'app-list-depots',
  templateUrl: './list-depots.component.html',
  styleUrls: ['./list-depots.component.css']
})
export class ListDepotsComponent implements OnInit  {
    private depots:Depot[];

    constructor(private _depotServise:DepotService, private _router:Router ) {

    }

    ngOnInit() {

        this._depotServise.getDepots().subscribe((depots)=>{
            this.depots=depots;
        },(error)=>{
            console.log(error);
        });
    }

    addDepot(){this._router.navigate(['/depots','depot','']);}

    updateDepot(id){

        this._router.navigate(['/depots','depot',id]);
    }

    deleteDepot(depot){
        this._depotServise.deleteDepot(depot.id).subscribe((data)=>{
            this.depots.splice(this.depots.indexOf(depot),1);
        },(error)=>{
            console.log(error);
        });
    }


}
