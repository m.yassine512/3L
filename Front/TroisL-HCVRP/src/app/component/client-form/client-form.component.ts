import { Component, OnInit } from '@angular/core';

import {ActivatedRoute, Router} from "@angular/router";
import {Client} from "../../client";
import {ClientService} from "../../services/client.service";
import {Coordinate} from "../../coordinate";

@Component({
  selector: 'app-client-form',
  templateUrl: './client-form.component.html',
  styleUrls: ['./client-form.component.css']
})
export class ClientFormComponent implements OnInit {
  private client:Client=new Client('',new Coordinate(0,0,''));
  private bol=true;
  private id:Number;

  constructor(private _clientServise:ClientService, private _router:Router,private _route:ActivatedRoute) { }

  ngOnInit() {
    this.callClient();
    }

  clientLocation(event){
  console.log(event);
  this.client.coordinateClient.latitude = event.coords.lat;
  this.client.coordinateClient.longitude = event.coords.lng;
  this.bol=true;
  }
  saveClient(){
    if(this.client.id==undefined){
        this._clientServise.createClient(this.client).subscribe((client)=>{
        console.log(client);
      },(error)=>{
        console.log(error);
      });
        this._router.navigate(["/"]);
    }else {this._clientServise.updateClient(this.client).subscribe((client)=>{
      console.log(client);
    },(error)=>{
      console.log(error);
    });this._router.navigate(["/"]);}
  }
  callClient(){

      this._route.params.subscribe((params) => {
          this.id = +params['id'];
          if(this.id){
              this._clientServise.getClient(this.id).subscribe((client)=>{
                  this.client=client;
              },(error)=>{
                  console.log(error);
              });
          }else{
              this.client.coordinateClient.latitude = 33.581053564669844;
              this.client.coordinateClient.longitude = -7.594315135130387;
              this.bol=false;
          }
      });
}
}
