import { Component, OnInit } from '@angular/core';
import {ClientService} from "../../services/client.service";
import {Router} from "@angular/router";

import {Client} from "../../client"
import {Coordinate} from "../../coordinate";
import {Waypoint} from "../../waypoint";

@Component({
  selector: 'app-list-clients',
  templateUrl: './list-clients.component.html',
  styleUrls: ['./list-clients.component.css']
})
export class ListClientsComponent implements OnInit {
    private clients:Client[];
    public origin: {};
    public destination: {};
    public waypoints: Waypoint[];
    public renderOptions = {
        suppressMarkers: true,
        color:'red'
    };

  constructor(private _clientServise:ClientService, private _router:Router ) {

      this.addCorrToWaypoints = this.addCorrToWaypoints.bind(this);

  }

  ngOnInit() {

    this._clientServise.getClients().subscribe((clients)=>{
        this.waypoints = new Array();
        this.clients=clients;
        this.clients.forEach(this.addCorrToWaypoints)
    },(error)=>{
      console.log(error);
    });

    this.getDirection();
    }

    addClient(){this._router.navigate(['/depots','depot','']);}

    addCorrToWaypoints(item){
        this.waypoints.push(new Waypoint(item.coordinateClient.latitude, item.coordinateClient.longitude));
    }

    updateClient1(id){this._router.navigate(['/clients','client',id]);}

    deleteClient(client){
        this._clientServise.deleteClient(client.id).subscribe((data)=>{
            this.clients.splice(this.clients.indexOf(client),1);
        },(error)=>{
            console.log(error);
        });
    }

    getDirection() {
        this.origin = {lat:33.581053564669844, lng:-7.594315135130387};
        this.destination = { lat:33.600215019581945, lng:-7.536121928343277};
    }


}
