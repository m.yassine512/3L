import { Component, OnInit } from '@angular/core';
import {Product} from "../../product";
import {ProductService} from "../../services/product.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Dimension} from "../../dimension";
import {NgModel} from "@angular/forms";

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {
    private product:Product=new Product(new Dimension(0,0,0));
    private id:Number;

  constructor(private _productService:ProductService, private _router:Router,private _route:ActivatedRoute) { }

  ngOnInit() {
    this.callProduct()
  }
    saveProduct(){
        if(!this.product.id){
            this._productService.createProduct(this.product).subscribe((product)=>{
                console.log(product);
            },(error)=>{
                console.log(error);
            });
            this._router.navigate(["/"]);
        }else {this._productService.updateProduct(this.product).subscribe((product)=>{
            console.log(product);
        },(error)=>{
            console.log(error);
        });this._router.navigate(["/"]);}
    }

    callProduct(){
        this._route.params.subscribe((params) => {
            this.id = +params['id'];
            if(this.id){
                this._productService.getProduct(this.id).subscribe((product)=>{
                    this.product=product;
                },(error)=>{
                    console.log(error);
                });
            }else{
                this.product = new Product(new Dimension(0,0,0));
            }
        });
    }

    volume() {
        this.product.volumeProduct = this.product.dimensionProduct.length *
            this.product.dimensionProduct.height *
            this.product.dimensionProduct.width;
    }
}
