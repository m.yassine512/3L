import { Component, OnInit } from '@angular/core';
import {Vehicle} from "../../vehicle";
import {VehicleService} from "../../services/vehicle.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-vehicle-form',
  templateUrl: './vehicle-form.component.html',
  styleUrls: ['./vehicle-form.component.css']
})
export class VehicleFormComponent implements OnInit {
    private vehicle:Vehicle;
    private id:Number;

  constructor(private _vehicleService:VehicleService, private _router:Router,private _route:ActivatedRoute) { }

  ngOnInit() {
    this.callVehicle();
  }

  saveVehicle(){
      if(!this.vehicle.id){
          this._vehicleService.createVehicle(this.vehicle).subscribe((vehicle)=>{
              console.log(vehicle);
          },(error)=>{
              console.log(error);
          });
          this._router.navigate(["/"]);
      }else {this._vehicleService.updateVehicle(this.vehicle).subscribe((vehicle)=>{
          console.log(vehicle);
      },(error)=>{
          console.log(error);
      });this._router.navigate(["/"]);}
  }

    callVehicle(){
        this._route.params.subscribe((params) => {
            this.id = +params['id'];
            if(this.id){
                this._vehicleService.getVehicle(this.id).subscribe((vehicle)=>{
                    this.vehicle=vehicle;
                },(error)=>{
                    console.log(error);
                });
            }else{
                this.vehicle = new Vehicle();
            }
        });
    }

}
