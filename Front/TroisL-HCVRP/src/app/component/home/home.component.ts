import { Component, OnInit } from '@angular/core';
import {Depot} from "../../depot";
import {Client} from "../../client";
import {DepotService} from "../../services/depot.service";
import {ClientService} from "../../services/client.service";
import {Router} from "@angular/router";
import {Vehicle} from "../../vehicle";
import {VehicleService} from "../../services/vehicle.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    private depots:Depot[];
    private clients:Client[];
    private vehicles:Vehicle[];

  constructor(private _depotServise:DepotService,private _clientServise:ClientService,private _vehicleServise:VehicleService, private _router:Router) { }

  ngOnInit() {
      this._depotServise.getDepots().subscribe((depots)=>{
          this.depots=depots;
      },(error)=>{
          console.log(error);
      });
      this._clientServise.getClients().subscribe((clients)=>{
          this.clients=clients;
      },(error)=>{
          console.log(error);
      });
      this._vehicleServise.getVehicles().subscribe((vehicles)=>{
          this.vehicles=vehicles;
      },(error)=>{
          console.log(error);
      });
  }

    updateClient1(id){this._router.navigate(['/clients','client',id]);}

    deleteClient(client){
        this._clientServise.deleteClient(client.id).subscribe((data)=>{
            this.clients.splice(this.clients.indexOf(client),1);
        },(error)=>{
            console.log(error);
        });
    }
    updateDepot(id){

        this._router.navigate(['/depots','depot',id]);
    }

    deleteDepot(depot){
        this._depotServise.deleteDepot(depot.id).subscribe((data)=>{
            this.depots.splice(this.depots.indexOf(depot),1);
        },(error)=>{
            console.log(error);
        });
    }
    updateVehicle(id){

        this._router.navigate(['/vehicles','vehicle',id]);
    }

    deleteVehicle(vehicle){
        this._vehicleServise.deleteVehicle(vehicle.id).subscribe((data)=>{
            this.vehicles.splice(this.vehicles.indexOf(vehicle),1);
        },(error)=>{
            console.log(error);
        });
    }
}
