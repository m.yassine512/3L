import {BaseEntity} from "./baseEntity";

export class Coordinate extends BaseEntity{

    latitude:Number;
    longitude:Number;
    adresse:string;

    constructor(latitude: Number, longitude: Number, adresse: string) {
        super();
        this.latitude = latitude;
        this.longitude = longitude;
        this.adresse = adresse;
    }
}