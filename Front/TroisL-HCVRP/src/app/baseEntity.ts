export class BaseEntity{
    id:Number;
    createdByUser:string;
    updatedByUser:string;

    createdOn:Date;
    updatedOn:Date;

    version:Number;

}