import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {HttpModule} from "@angular/http";
import {FormsModule} from "@angular/forms";
import {AgmCoreModule} from "@agm/core";
import {AgmDirectionModule} from 'agm-direction'
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';

import {AppComponent } from './app.component';
import {ClientService} from "./services/client.service";
import {DepotService} from "./services/depot.service";
import {ListClientsComponent} from "./component/list-clients/list-clients.component";
import {ClientFormComponent} from "./component/client-form/client-form.component";
import {ListVehiclesComponent} from "./component/list-vehicles/list-vehicles.component";
import { ListDepotsComponent } from './component/list-depots/list-depots.component';
import { DepotFormComponent } from './component/depot-form/depot-form.component';
import { HomeComponent } from './component/home/home.component';
import { VehicleFormComponent } from './component/vehicle-form/vehicle-form.component';
import { ProductFormComponent } from './component/product-form/product-form.component';
import { ListProductsComponent } from './component/list-products/list-products.component';


const appRoute:Routes=[
  {path:'home',component:HomeComponent},
  {path:'',
      redirectTo: 'home',
      pathMatch: 'full'},
  {path:'clients',component:ListClientsComponent},
    {path:'clients/client/:id', component:ClientFormComponent},
    {path:'clients/client', component:ClientFormComponent},
  {path:'vehicles',component:ListVehiclesComponent},
    {path:'vehicles/vehicle/:id', component:VehicleFormComponent},
    {path:'vehicles/vehicle', component:VehicleFormComponent},
  {path:'depots',component:ListDepotsComponent},
    {path:'depots/depot/:id', component:DepotFormComponent},
    {path:'depots/depot', component:DepotFormComponent},
  {path:'products',component:ListProductsComponent},
    {path:'products/product/:id', component:ProductFormComponent},
    {path:'products/product', component:ProductFormComponent}


];

@NgModule({
  declarations: [
    AppComponent,
    ListClientsComponent,
    ClientFormComponent,
    ListVehiclesComponent,
    ListDepotsComponent,
    DepotFormComponent,
    HomeComponent,
    VehicleFormComponent,
    ProductFormComponent,
    ListProductsComponent
  ],
  imports: [
    HttpModule,
    BrowserModule,
    RouterModule.forRoot(appRoute),
    AgmCoreModule.forRoot({
          apiKey: 'AIzaSyD6IBiXhFTu0nSyACTa3S7oYRCB9CY7IJg'
      }),
      AgmDirectionModule,
      SlimLoadingBarModule,
      FormsModule
  ],
  providers: [ClientService,DepotService],
  bootstrap: [AppComponent]
})
export class AppModule { }
