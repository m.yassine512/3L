import {BaseEntity} from "./baseEntity";

export class Vehicle extends BaseEntity {

    nameVehicle:string;
    matricule:string;
    maxWeight:Number;
    fixedCost:Number;
    operatingCostPerMetre:Number;
    volumeVehicle:Number;
}