import {BaseEntity} from "./baseEntity";

export class Dimension extends BaseEntity {

    length:number;
    width:number;
    height:number;

    constructor(length: number, width: number, height: number) {
        super();
        this.length = length;
        this.width = width;
        this.height = height;
    }
}