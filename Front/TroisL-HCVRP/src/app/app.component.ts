import {Component, Inject} from '@angular/core';
import {DOCUMENT} from "@angular/common";
import {SlimLoadingBarService} from 'ng2-slim-loading-bar';
import { NavigationCancel,
    Event,
    NavigationEnd,
    NavigationError,
    NavigationStart,
    Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  constructor(private _loadingBar: SlimLoadingBarService, private _router: Router, @Inject(DOCUMENT) private document: Document){
      this._router.events.subscribe((event: Event) => {
          this.navigationInterceptor(event);
      });
  }

    private navigationInterceptor(event: Event): void {
        if (event instanceof NavigationStart) {
            this._loadingBar.start();
        }
        if (event instanceof NavigationEnd) {
            this._loadingBar.complete();
        }
        if (event instanceof NavigationCancel) {
            this._loadingBar.stop();
        }
        if (event instanceof NavigationError) {
            this._loadingBar.stop();
        }
    }

  public Toggler(){

    if (this.document.body.classList.contains("sidenav-toggled")){
        this.document.body.classList.remove("sidenav-toggled");
    }else {this.document.body.classList.add("sidenav-toggled");}
  }
}
