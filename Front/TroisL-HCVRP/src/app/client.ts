import {BaseEntity} from "./baseEntity";
import {Coordinate} from "./coordinate";

export class Client extends BaseEntity{
  nameClient:string;
  coordinateClient:Coordinate;

    constructor(nameClient: string, coordinateClient: Coordinate) {
        super();
        this.nameClient = nameClient;
        this.coordinateClient = coordinateClient;
    }
}
